data "ignition_file" "redis_container_unit" {
  path = "/home/${var.username}/.config/containers/systemd/redis.container"
  content {
    content = templatefile("${path.module}/templates/redis.container", {})
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_config" "config" {
  files = [
    data.ignition_file.redis_container_unit.rendered
  ]
}
