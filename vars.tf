variable "username" {
  type = string
}

variable "uid" {
  type = number
}

variable "gid" {
  type = number
}
